var app = angular.module('MainModule', [])
    // inject the service factory into our controller

var SERVICE = SERVICE || {};
SERVICE.mainController = function($scope, $http) {
    var _this = this;
    $scope.isAPIReady = false;
    $scope.isLoadingPhotos = true;
    $scope.photos = [];
    this.access_token = "";

    $scope.fetchData = function() {
        var url = 'https://api.instagram.com/oauth/authorize/?client_id=fe1a339d0c6a4dbf84ce3dc7299230cc&redirect_uri=http://localhost:8000/&response_type=token&scope=public_content'
        $http.post(url)
            .success(function(data) {
                console.log("success");
                console.log(data);
            }).
        error(function(data) {
            console.log("errrrrr");
            console.log(data);
        });
    }

    $scope.$on("urlLocationChanged", function(event, data) {
        if (data.location.search('access_token') != -1) {
            // access token is present and itialized map here
            _this.access_token = data.location.getUrlVar("/access_token");
            $scope.isAPIReady = true;
            var map = new SERVICE.GoogleMapService($scope);
            map.initMap(document.getElementById('googleMap'));
            map.onLocationChange($scope.onLocationChange);
           $scope.fetchProfileData();
        }
    });

    $scope.fetchProfileData = function(){
        var url = "http://localhost:8000/record?access_token="+_this.access_token;
        var _data = {
            access_token : _this.access_token
        }
       $http({
            method  : 'GET',
            url: url,
            data: _data
        })
        .success(function(response) {
            //console.log("success");
            //console.log(response);

            if( response.data != undefined && response.data != null && response.data !== ''){
                $scope.user = response.data;
                console.log($scope.user)
            }
        })
        .error(function(data) {
            //console.log("errrrrr");
            console.log(data);
        });
    }

    $scope.onLocationChange = function(data){
        $scope.isLoadingPhotos = true;
        var location = {
            lat : data.lat(),
            lng : data.lng()
        }
        var url = "http://localhost:8000/record/photos?access_token="+_this.access_token +"&lat="+location.lat + "&lng="+location.lng;
        var _data = {
            access_token : _this.access_token,
            location : location
        }
        $http({
            method  : 'GET',
            url: url,
            data: _data
        })
        .success(function(response) {
            console.log("success");
            console.log(response);
            $scope.isLoadingPhotos = false;
            if( response != undefined && response != null && response !== ''){
                //sss.data[0].images
                //$scope.photos
                $scope.photos = [];
                try{
                    for( var i = 0 ; i < response.length; i++){
                        var data = response[i].data;

                        for( var j = 0 ; j < data.length; j++){
                            if( data[i].images != undefined && data[i].images !== ""){
                                $scope.photos.push( data[i].images.thumbnail.url);
                            }
                        }
                    }
                } catch(e){
                }

            }
        })
        .error(function(data) {
            $scope.isLoadingPhotos = false;
            console.log("errrrrr");
            console.log(data);
        });

    }
}

SERVICE.mainController.$inject = ['$scope', '$http'];
app.controller('mainController', SERVICE.mainController);