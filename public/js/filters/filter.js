var app = angular.module('filters', []);
app.filter('capitalizeFirstLetter', function() {
    return function(string) {
        try {
            var split = string.split(' ');
            var finalStr = '';
            finalStr = split[0].charAt(0).toUpperCase() + split[0].slice(1).toLowerCase();
            for( var i = 1; i < split.length;i++){
                finalStr +=  ' '+split[i];
            }
            return finalStr;
        } catch (e) {
            return string;
        }
    }
});
app.filter('formatMobileNumber', function() {
    return function(data){
        var ndata = data.toString();
        if(ndata.length > 0){
            ndata = '0'+ndata;
            if(ndata.length > 4){
                ndata = ndata.substr(0,4) + '-' +ndata.substr(4,ndata.length - 4)
            }
        }
        return ndata;
    }
});