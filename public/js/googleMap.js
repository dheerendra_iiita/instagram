var SERVICE = SERVICE || {};

SERVICE.GoogleMapService = function(scope) {
    this.marker = null;
    this.defaultLat = 28.6139;
    this.defaultLng = 77.2090;
    this.map = null;
    this.marker = null;
    this.refCallback = null;
    this.scope = scope;
}

SERVICE.GoogleMapService.prototype.constructor = SERVICE.GoogleMapService;

SERVICE.GoogleMapService.prototype.initMap = function(elem) {
    google.maps.event.addDomListener(window, 'load', this.addEvents.bind(this, elem));
}
SERVICE.GoogleMapService.prototype.addEvents = function(elem) {
    var _this = this;
    this.map = new google.maps.Map(elem, {
        zoom: 13,
        center: {
            lat: _this.defaultLat,
            lng: _this.defaultLng
        }
    });

    this.marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: {
            lat: _this.defaultLat,
            lng: _this.defaultLng
        }
    });

    this.marker.addListener('click', this.toggleBounce.bind(this));

    this.map.addListener("idle", function() {
        _this.marker.setVisible(true);
        var data = _this.map.getCenter();
        var lat = data.lat();
        var lng = data.lng();

        /*        	var latlng = new google.maps.LatLng(lat, lng);
                	 marker.setPosition(latlng);*/
        this.marker.setPosition(data);
        if( _this.refCallback !== null ){
        	_this.refCallback(data);
        } else {
        	console.log(111111);
        }

    }.bind(this));

    _this.map.addListener("dragstart", function() {

        _this.marker.setVisible(false);

    })
}

SERVICE.GoogleMapService.prototype.onLocationChange = function(callback){
	this.refCallback = callback;
}

SERVICE.GoogleMapService.prototype.toggleBounce = function() {
    if (this.marker.getAnimation() !== null) {
        this.marker.setAnimation(null);
    } else {
        this.marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}