angular.module('mainApp', ['ui.router','ngMaterial', 'ngAnimate', 'ngAria', 'ngMdIcons', 'ngMessages', 'MainModule'])
.config(['$httpProvider', function($httpProvider) {
   // $urlRouterProvider.otherwise('/home');
    /*$stateProvider
    .state('home', {
        url: '/home',
        templateUrl: BASE_URL+'userdata.html'
    })*/

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]).
constant("BASE_URL" , './')
.run(function ($rootScope, $urlRouter, $location) {
     $rootScope.$on('$locationChangeSuccess', function(e) {
        console.log(11111)
        $rootScope.$broadcast('urlLocationChanged', {location : $location.path()});
     });
});


var SERVICE = SERVICE || {};
SERVICE.GlobalData = function(){
    
}
SERVICE.GlobalData.prototype.customSocketTry = function(type){
    if( type.toUpperCase() === 'USER'){
        return io.connect('http://site.com:/user', {path: '/api/socket.io'});
    } else if(type.toUpperCase() === 'CABBIE'){
        return io.connect('http://site.com:/cabbie', {path: '/api/socket.io'});
    }
}

Array.prototype.removeAtIndex = function(index) {
    var splice = this.splice(index);
    splice.shift();
    for (var i = 0; i < splice.length; i++) {
        this.push(splice[i]);
    }
};

String.prototype.getUrlVar = function(requestedKey) {
    "use strict";
    var vars = [], hashes, hash,
        temp_hashes,
        i;
    temp_hashes = this.replace("#", "");
    hashes = temp_hashes.split('&');
    for (i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    if (typeof requestedKey === 'undefined') {
        return vars;
    } else {
        return vars[requestedKey];
    }
}