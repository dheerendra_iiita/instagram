var SERVER = SERVER || {};

SERVER.SendResponse = function(response,data){
    this._response = response;
    this._data = data;
}
SERVER.SendResponse.prototype.send = function(){
    this._response.status(200).send(this._data);
    this._response.end();
}

module.exports = SERVER.SendResponse;