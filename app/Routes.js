var ResponseHandler = require('./ResponseHandler');
var api_request = require('request');

module.exports = function (app) {

    var baseURL = 'https://api.instagram.com'
    // add a record
    app.get('/record', function (request, response) {
        var _response = response;
        var inputData = {};
        inputData.method = request.method;
        var access_token = getUrlVar(request.originalUrl, '/record?access_token');

        api_request(baseURL+'/v1/users/self/?access_token='+access_token, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            //console.log(body) // Show the HTML for the Google homepage.
                var sender = new ResponseHandler(_response, body);
                sender.send();
          } else {
            var sender = new ResponseHandler(_response, body);
                sender.send();
          }
        }.bind(this));
        
    });
    // add a record
    app.get('/record/photos', function (request, response) {
        var _response = response;
        var inputData = request.body;
        inputData.method = request.method;
        var access_token = getUrlVar(request.originalUrl, '/record/photos?access_token');
        var lat = getUrlVar(request.originalUrl, 'lat');
        var lng = getUrlVar(request.originalUrl, 'lng');

        var locationsIDs = [];
        var searchedItems = [];

        var URL = baseURL+"/v1/locations/search?lat="+lat+"&lng="+lng+"&access_token="+access_token;
        api_request(URL, function (error, response, body) {
            body = JSON.parse(body)
            if( body.data !== undefined ){
                var ldata = body["data"];
                for( var i = 0 ; i  < ldata.length; i++){
                    if( ldata[i].id !== undefined && ldata[i].id != 0)
                        locationsIDs.push( ldata[i].id);
                }

                
                getAllPhotos();
                //"https://api.instagram.com/v1/locations/278563802/media/recent?access_token=3598170061.fe1a339.a36b5a3196724414b80ea1fb60e5541a"
                

                var count = 0;
                function getAllPhotos(){
                    var URL = baseURL+"/v1/locations/"+locationsIDs[count] +"/media/recent?access_token="+access_token;
                    count++;

                    api_request(URL, function (error, response, body) {
                        if( count < locationsIDs.length ){
                            try{
                                body = JSON.parse(body)
                                searchedItems.push(body);
                            } catch(e){
                                
                            }
                            getAllPhotos();
                        } else {
                            // send data from here
                            var sender = new ResponseHandler(_response, body);
                            sender.send();
                        }
                    });
                }
            }
        });
    });
    // delete a record
    app.delete('/api/record/:_id', function (request, response) {
        var inputData = request.body;
        inputData.method = request.method;
        inputData.index = request.params._id
        //console.log(inputData);
        var sender = new ResponseHandler(response, inputData);
        sender.send()        
    });

    // application -------------------------------------------------------------
    app.get('*', function (req, res) {
        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

    

    function getUrlVar(url, requestedKey) {
        "use strict";
        var vars = [], hashes, hash,
            temp_hashes,
            i;
        temp_hashes = url.replace("#", "");
        hashes = temp_hashes.split('&');
        for (i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        if (typeof requestedKey === 'undefined') {
            return vars;
        } else {
            return vars[requestedKey];
        }
    }
};

